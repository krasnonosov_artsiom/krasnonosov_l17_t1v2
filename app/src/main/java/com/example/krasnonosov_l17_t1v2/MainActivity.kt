package com.example.krasnonosov_l17_t1v2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.adapters.Item
import com.example.adapters.MyDiffUtil
import com.example.adapters.RecyclerAdapter
import com.example.dialog.DialogCreator
import com.example.generation.ItemGenerator
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var items: ArrayList<Item> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var id = 1
        var itemGenerator = ItemGenerator()
        for (n in 1..5) {
            items.add(itemGenerator.createItem(id))
            id++
        }

        val oldItems = ArrayList(items)

        val adapter = RecyclerAdapter(items)
        val dialogCreator = DialogCreator(this, adapter)
        recyclerView.adapter = adapter
        val manager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = manager



        floatingActionButton.setOnClickListener {
            dialogCreator.addDialog(items)
            oldItems.clear()
            oldItems.addAll(items)
        }

        Log.i("oldListSize", oldItems.size.toString())
        Log.i("newListSize", items.size.toString())

        swipeToRefresh.setOnRefreshListener {
//            val myDiffUtil = DiffUtil.calculateDiff(MyDiffUtil(oldItems, items))
//            myDiffUtil.dispatchUpdatesTo(adapter)
//            oldItems.clear()
//            oldItems.addAll(items)
            swipeToRefresh.isRefreshing = false
        }
    }


}