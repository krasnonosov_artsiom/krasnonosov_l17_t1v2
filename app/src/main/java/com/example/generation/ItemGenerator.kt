package com.example.generation

import com.example.adapters.Item
import com.example.krasnonosov_l17_t1v2.R
import java.util.*

class ItemGenerator {

    private val titles = arrayOf("Room", "House", "Apartments", "Cottage")
    private val descriptions = arrayOf("House in respectable district. There is park near",
        "Without furniture. Repair on account of rent.",
        "Metro is near. There are TV  in room. Without pets")
    private val images = arrayOf(R.drawable.room_icon, R.drawable.house_icon, R.drawable.home_icon, R.drawable.cottege_icon)

    fun createItem(id: Int) : Item {
        val apartment = Random().nextInt(4)

        return Item(id, images[apartment], titles[apartment], descriptions[Random().nextInt(descriptions.size)])
    }

}