package com.example.dialog

import android.app.AlertDialog
import android.content.Context
import android.widget.EditText
import androidx.recyclerview.widget.DiffUtil
import com.example.adapters.Item
import com.example.adapters.MyDiffUtil
import com.example.adapters.RecyclerAdapter
import com.example.generation.ItemGenerator

class DialogCreator(private val context: Context, private val adapter: RecyclerAdapter) {

    private var dialog = AlertDialog.Builder(context)

    fun addDialog(items: ArrayList<Item>) {
        val itemGenerator = ItemGenerator()
        val editText = EditText(context)
        dialog.setTitle("Add element")
        dialog.setMessage("How much elements add?")
        dialog.setView(editText)
        dialog.setPositiveButton("Add") { _, _ ->

            val elementsAmount = editText.text.toString().toInt()
            for (x in 0 until elementsAmount) {
                var oldItems = ArrayList(items)
                items.add(itemGenerator.createItem(if (items.size != 0) items[items.size - 1].id else 1 ))
                val myDiffUtil = DiffUtil.calculateDiff(MyDiffUtil(oldItems, items))
                myDiffUtil.dispatchUpdatesTo(adapter)
            }

        }
        dialog.show()
    }


}