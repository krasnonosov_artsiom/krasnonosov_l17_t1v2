package com.example.adapters

class Item(val id: Int, val image: Int, val title: String, val description: String)