package com.example.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.krasnonosov_l17_t1v2.R

class RecyclerAdapter(private var items: ArrayList<Item>) : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    interface OnDeleteListener {
        fun onClick(position: Int)
    }

    private var oldItems = ArrayList(items)

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val title: TextView = itemView.findViewById(R.id.titleTextView)
        private val description: TextView = itemView.findViewById(R.id.descriptionTextView)
        private val image: ImageView = itemView.findViewById(R.id.realtyImage)
        val deleteButton: ImageButton = itemView.findViewById(R.id.deleteButton)

        fun bind(item: Item) {
            title.text = item.title
            description.text = item.description
            image.setImageResource(item.image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])

        holder.deleteButton.setOnClickListener {
            items.removeAt(position)
            notifyItemRemoved(holder.adapterPosition)
            notifyItemRangeChanged(position, itemCount)
        }

//        holder.deleteButton.setOnClickListener { listener.onClick(holder.adapterPosition) }
    }
}